package Locations;

import org.rspeer.runetek.api.commons.BankLocation;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;

public enum Location
{
    AIR(Area.rectangular(new Position(3009, 3358, 0), new Position(3018, 3355, 0)),
            Area.rectangular(new Position(2981, 3296, 0), new Position(2990, 3288, 0)),
            Area.rectangular(new Position(2339, 4826, 0), new Position(2849, 4839, 0)),
            BankLocation.FALADOR_EAST,
            5527,
            556),

    EARTH(Area.rectangular(new Position(3250, 3422, 0), new Position(3257, 3419, 0)),
            Area.rectangular(new Position(3300, 3469, 0), new Position(3308, 3477, 0)),
            Area.rectangular(new Position(2651, 4849, 0), new Position(2668, 4830, 0)),
            BankLocation.VARROCK_EAST,
            5535,
            557),

    BODY(Area.rectangular(new Position(3098, 3499, 0), new Position(3091, 3488, 0)),
            Area.rectangular(new Position(3059, 3452, 0), new Position(3046, 3438, 0)),
            Area.rectangular(new Position(2514, 4835, 0), new Position(2531, 4849, 0)),
            BankLocation.EDGEVILLE,
            5533,
            559);


    public final Area bankArea;
    public final Area ruinsArea;
    public final Area altarArea;
    public final BankLocation bankLocation;
    public final int tiaraId;
    public final int runeId;

    Location(final Area bankArea, final Area ruinsArea, final Area altarArea, final BankLocation bankLocation, int tiaraId, int runeId)
    {
        this.bankArea = bankArea;
        this.ruinsArea = ruinsArea;
        this.altarArea = altarArea;
        this.bankLocation = bankLocation;
        this.tiaraId = tiaraId;
        this.runeId = runeId;
    }
}
