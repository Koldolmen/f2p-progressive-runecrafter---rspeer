package Locations;

import org.rspeer.runetek.api.commons.BankLocation;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;

public interface RcArea
{
    public static final Area BANK_AREA = Area.rectangular(new Position(3009, 3358, 0), new Position(3018, 3355, 0));
    public static final Area RUINS_AREA = Area.rectangular(new Position(2981, 3296, 0), new Position(2990, 3288, 0));
    public static final Area ALTAR_AREA = Area.rectangular(new Position(2339, 4826, 0), new Position(2849, 4839, 0));
    public static final BankLocation BANK = BankLocation.VARROCK_EAST;
    public static final int TIARA_ID = 5527;
    public static final int RUNE_ID = 556;
}
