import Locations.*;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.EquipmentSlot;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.Script;
import org.rspeer.script.ScriptMeta;
import org.rspeer.ui.Log;


@ScriptMeta(developer = "Koldolmen", desc = "Air, earth, body runes", name = "F2pRcer")
public class F2PRcer extends Script
{
    private Location location;
    private int xp = 0;

    @Override
    public void onStart()
    {
        setLocation();
        xp = Skills.getExperience(Skill.RUNECRAFTING);
    }

    @Override
    public int loop()
    {

        if (!Movement.isRunEnabled() && Movement.getRunEnergy() > 80)
        {
            Movement.toggleRun(true);
        }

        if (EquipmentSlot.HEAD.getItemId() != location.tiaraId)
        {
            if (!location.bankArea.contains(Players.getLocal()))
            {
                Bank.open(location.bankLocation);
                return 1500;
            }
            else
            {
                Bank.open();
            }
            if (Bank.isOpen())
            {
                Bank.depositInventory();

                if (Bank.contains(location.tiaraId))
                {
                    Bank.withdraw(location.tiaraId, 1);
                    Time.sleepUntil(() -> Inventory.contains(location.tiaraId), 500);
                    Bank.close();
                    Time.sleepUntil(() -> Bank.isClosed(), 500);
                    Item tiara = Inventory.getFirst(location.tiaraId);
                    if (tiara != null)
                    {
                        tiara.interact("Wear");
                        return 1000;
                    }
                    else
                    {
                        Log.info("No tiara found in inventory");
                    }
                }
                else
                {
                    Log.info("No tiara found in bank");
                    setStopping(true);
                }
            }
        }
        else if (Inventory.contains(7936))
        {
            if (location.altarArea.contains(Players.getLocal()))
            {
                SceneObject altar = SceneObjects.getNearest("Altar");
                if (altar != null)
                {
                    altar.interact("Craft-rune");
                    Time.sleepUntil(() -> Inventory.contains(location.runeId), 2000);
                }
                else
                {
                    Log.info("hittade inte altaret");
                }
            }

            if (!location.ruinsArea.contains(Players.getLocal()))
            {
                Movement.walkTo(location.ruinsArea.getCenter());
                return 3000;
            }
            else
            {
                SceneObject ruins = SceneObjects.getNearest("Mysterious ruins");
                if (ruins != null)
                {
                    ruins.interact("Enter");
                    Time.sleepUntil(() -> location.altarArea.contains(Players.getLocal()), 2000);
                }
            }
        }
        else if (Inventory.contains(location.runeId))
        {
            if (location.altarArea.contains(Players.getLocal()))
            {
                SceneObject portal = SceneObjects.getNearest("Portal");
                if (portal != null)
                {
                    portal.interact("Use");
                    Time.sleepUntil(() -> location.ruinsArea.contains(Players.getLocal()), 2000);
                    setLocation();
                }
                else
                {
                    Log.info("Couldn't find portal");
                }
            }
            else if (!location.bankArea.contains(Players.getLocal()))
            {
                Bank.open(location.bankLocation);
                return 3000;
            }
            else
            {
                Bank.open();
            }

            if (Bank.isOpen())
            {
                Bank.depositInventory();
                Time.sleepUntil(() -> Inventory.isEmpty(), 500);
                Bank.withdrawAll(7936);
            }
        }
        else if (Inventory.isEmpty())
        {
            if (!location.bankArea.contains(Players.getLocal()))
            {
                Bank.open(location.bankLocation);
                return 1500;
            }
            else
            {
                Bank.open();
                Time.sleepUntil(() -> Bank.isOpen(), 1000);
            }

            if (Bank.isOpen())
            {
                if (Inventory.containsAnyExcept(7936))
                {
                    Bank.depositInventory();
                }
                else if (Bank.contains(7936))
                {
                    Bank.withdrawAll(7936);
                }
                else
                {
                    Log.info("No essence left in bank");
                    setStopping(true);
                }
            }

        }
        else
        {
            Log.info("Resetting");

            if (!location.bankArea.contains(Players.getLocal()))
            {
                Bank.open(location.bankLocation);
            }
            else
            {
                Bank.open();
            }
            if (Bank.isOpen())
            {
                Bank.depositInventory();
                Bank.depositEquipment();

                if (Bank.contains(location.tiaraId))
                {
                    Bank.withdraw(location.tiaraId, 1);
                    Time.sleepUntil(() -> Inventory.contains(location.tiaraId), 500);
                    Bank.close();
                    Time.sleepUntil(() -> Bank.isClosed(), 500);
                    Item tiara = Inventory.getFirst(location.tiaraId);
                    if (tiara != null)
                    {
                        tiara.interact("Wear");
                        return 1000;
                    }
                    else
                    {
                        Log.info("No tiara found in inventory");
                    }
                }
                else
                {
                    Log.info("No tiara found in bank");
                    setStopping(true);
                }
            }
        }
        return 300;
    }

    private void setLocation()
    {
        int currentLevel = Skills.getCurrentLevel(Skill.RUNECRAFTING);

        if (currentLevel < 14)
        {
            location = location.AIR;
        }
        else if (currentLevel < 20)
        {
            location = location.EARTH;
        }
        else
        {
            location = location.BODY;
        }
    }

    @Override
    public void onStop()
    {
        Log.info("Xp gained: " + (Skills.getExperience(Skill.RUNECRAFTING) - xp));
    }
}